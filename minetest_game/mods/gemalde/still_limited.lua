-- Count the number of pictures.
local function get_picture(number)
	local filename	= minetest.get_modpath("gemalde").."/textures/gemalde_limited_"..number..".png"
	local file		= io.open(filename, "r")
	if file ~= nil then io.close(file) return true else return false end
end

local N = 1

while get_picture(N) == true do
	N = N + 1
end

N = N - 1

-- register for each picture
for n=1, N do

local groups = {choppy=2, dig_immediate=3, picture=1, not_in_creative_inventory=1}
if n == 1 then
	groups = {choppy=2, dig_immediate=3, picture=1}
end

-- node
minetest.register_node("gemalde:node_limited_"..n.."", {
	description = "Picture (limited to one try) #"..n.."",
	drawtype = "signlike",
	tiles = {"gemalde_limited_"..n..".png"},
	visual_scale = 3.0,
	inventory_image = "gemalde_node.png",
	wield_image = "gemalde_node.png",
	paramtype = "light",
	paramtype2 = "wallmounted",
	sunlight_propagates = true,
	walkable = false,
	selection_box = {
		type = "wallmounted",
	},
	groups = groups,

-- Answer gives reward mechanism (core) (rom 20180531)

	after_place_node = function(pos, placer)
		minetest.get_meta(pos):set_string("owner", placer:get_player_name());
	end,
--[[
	on_receive_fields = function(pos, formname, fields, sender)
		local meta = minetest:get_meta(pos);
		local name = sender:get_player_name();

		error(fields);

		if formname == "set_answer" then --(name == meta:get_string("owner"))
			-- TODO: (maybe) sanitize input
			meta:set_string("answer", fields.answer);

		else if meta:get_string("answer") == fields.answer
				and fields.answer ~= ""
                and meta:get_string("rewarded_"..name) ~= "true" then
	--			and meta:get_string("failed_"..name) ~= "true"
		
			sender:get_inventory():add_item("craft","locks:keychain 1");
			-- TODO: (IMPORTANT) set reward before use
			-- TODO: (later) set reward from meta from field
			meta:set_string("rewarded_"..name, "true");

	--	else
	--		meta:set_string("failed_"..name, "true");
		end end
	end,
]]
	on_rightclick = function(pos, node, clicker)

-- Answer gives reward mechanism (form)
	
		if minetest.check_player_privs(clicker, {teacher=true}) then
			-- TODO: (later) add field for setting the reward
			minetest.show_formspec(clicker:get_player_name(), "set_answer", 				
				"size[8, 4]" ..
				"label[0, 0;reponse]" ..
				"field[1,2;7,1;answer;;]" ..
				-- ça marche pas "button_exit[3,3;5,1;next_image;Prochaine image]" ..
				"button_exit[0,3;2,1;set;Enregistrer]"
		    	)

				minetest.register_on_player_receive_fields( function(sender, formname, fields)
					local meta = minetest.get_meta(pos);
					local name = sender:get_player_name();
					if formname == "set_answer" then --(name == meta:get_string("owner"))
						-- TODO: (maybe) sanitize input
						--[[if fields.next_image == true then
							local length = string.len (node.name)
							local number = string.sub (node.name, 14, length)
							if number == tostring(N) then
								number = 1
							else
								number = number + 1
							end
							print("[gemalde] number is "..number.."")
							node.name = "gemalde:node_"..number..""
							minetest.env:set_node(pos, node)
						else ETC]]
						meta:set_string("answer", fields.answer);
					end
				end)
				return
		else if not minetest.check_player_privs(clicker, {painter=true}) then
                if minetest.get_meta(pos):get_string("rewarded_"..clicker:get_player_name()) == "true" then return end
                if minetest.get_meta(pos):get_string("failed_"..clicker:get_player_name()) == "true" then return end
			minetest.show_formspec(clicker:get_player_name(), "input_answer", 				
				"size[8, 4]" ..
				"label[0, 0;Donnez votre réponse]" ..
				"field[1,2;7,1;answer;;]" ..
				-- ça marche pas "button[3,3;5,1;cancel;Annuler]" ..
				"button_exit[0,3;2,1;set;Enregistrer]"
		    	)

				minetest.register_on_player_receive_fields( function(sender, formname, fields)
					local meta = minetest.get_meta(pos);
					local name = sender:get_player_name();
					if fields.cancel == true then return end
					if fields.answer == nil then fields.answer = "" end
					if formname == "input_answer"
							and meta:get_string("answer") == fields.answer
							and fields.answer ~= ""
							and meta:get_string("rewarded_"..name) ~= "true"
							and meta:get_string("failed_"..name) ~= "true" then
	
						sender:get_inventory():add_item("main","default:diamond 1");
					
						-- TODO: (IMPORTANT) set reward before use
						-- TODO: (later) set reward from meta from field
						meta:set_string("rewarded_"..name, "true");


					else if formname == "input_answer"
							and meta:get_string("answer") ~= fields.answer
							and fields.answer ~= "" then
						meta:set_string("failed_"..name, "true");
					end end
				end)
				return
				
		end end

		local length = string.len (node.name)
		local number = string.sub (node.name, 22, length)

		-- TODO. Reducing currently not working, because sneaking prevents right click.
		local keys=clicker:get_player_control()
		if keys["sneak"]==false then
			if number == tostring(N) then
				number = 1
			else
				number = number + 1
			end
		else
			if number == 1 then
				number = N - 1
			else
				number = number - 1
			end
		end

		print("[gemalde] number is "..number.."")
		node.name = "gemalde:node_limited_"..number..""
		minetest.env:set_node(pos, node)
	end,



--	TODO.
--	on_place = minetest.rotate_node
})

-- crafts
if n < N then
minetest.register_craft({
	output = 'gemalde:node_limited_'..n..'',
	recipe = {
		{'gemalde:node_limited_'..(n+1)..''},
	}
})
end

n = n + 1

end

-- close the craft loop


minetest.register_craft({
	output = 'gemalde:node_limited_'..N..'',
	recipe = {
		{'gemalde:node_limited_1'},
	}
})
